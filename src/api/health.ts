import axios from 'axios';
import config from '../config';

export const checkHealth = async () => {
  try {
    const response = await axios.get(`${config.apiUrl}/health`);
    return response.data;
  } catch (error) {
    console.error('Error checking health:', error);
    throw error;
  }
};
