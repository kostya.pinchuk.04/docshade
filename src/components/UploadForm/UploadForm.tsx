import React from 'react';
import { useFormik } from 'formik';
import { fileValidationSchema } from '../../utils/Validator';
import { handlePDFUpload } from '../../services/pdfService';
import { Form, Input, ErrorMessage, Title, UploadButton } from './UploadForm.styles';

const UploadForm: React.FC = () => {
  const formik = useFormik({
    initialValues: {
      file: null,
    },
    validationSchema: fileValidationSchema,
    onSubmit: async (values) => {
      if (values.file) {
        try {
          const response = await handlePDFUpload(values.file);
          console.log('Upload successful:', response);
        } catch (error) {
          console.error('Error uploading file:', error);
        }
      }
    },
  });

  return (
    <Form onSubmit={formik.handleSubmit}>
      <Title>Upload PDF</Title>
      <Input
        id="file"
        name="file"
        type="file"
        onChange={(event) => {
          const file = event.currentTarget.files ? event.currentTarget.files[0] : null;
          formik.setFieldValue('file', file);
        }}
      />
      {formik.errors.file ? <ErrorMessage>{formik.errors.file}</ErrorMessage> : null}
      <UploadButton type="submit">Upload</UploadButton>
    </Form>
  );
};

export default UploadForm;
