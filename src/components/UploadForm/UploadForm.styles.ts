import styled from 'styled-components';

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 16px;
  margin: 0 auto;
  max-width: 400px;
  background-color: #f9f9f9;
  border-radius: 8px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

export const Input = styled.input`
  margin: 16px 0;
  padding: 8px;
  width: 100%;
  border: 1px solid #ccc;
  border-radius: 4px;
  font-size: 16px;
`;

export const ErrorMessage = styled.div`
  color: red;
  font-size: 14px;
  margin-top: -12px;
  margin-bottom: 12px;
`;

export const Title = styled.h1`
  font-size: 24px;
  color: #333;
  margin-bottom: 24px;
`;

export const UploadButton = styled.button`
  padding: 12px 24px;
  font-size: 16px;
  color: white;
  background-color: #007bff;
  border: none;
  border-radius: 4px;
  cursor: pointer;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #0056b3;
  }
`;
