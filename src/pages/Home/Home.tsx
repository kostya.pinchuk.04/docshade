import React, { useEffect } from 'react';
import UploadForm from '../../components/UploadForm/UploadForm';
import { checkHealth } from '../../api/health';
import { Container } from './Home.styles';

const Home: React.FC = () => {
  useEffect(() => {
    const fetchHealthStatus = async () => {
      try {
        const response = await checkHealth();
        console.log('Health status:', response);
      } catch (error) {
        console.error('Error checking health status:', error);
      }
    };

    fetchHealthStatus();
  }, []);

  return (
    <Container>
      <h1>Upload PDF</h1>
      <UploadForm />
    </Container>
  );
};

export default Home;
